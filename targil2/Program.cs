﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log4netMssql
{
    class Program
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            /*
            my_logger.Info("******************** System startup");
            my_logger.Debug("This is a debug message");
            my_logger.Warn("This is a warning message");
            my_logger.Error("This is an ERROR message");
            my_logger.Fatal("This is a FATAL message");
            my_logger.Info("******************** System shutdown");
            */

            my_logger.Info("******************** System startup");

            Console.WriteLine("Enter 1st number:");
            double num1 = double.NaN;
            string user_input = "";
            while (double.IsNaN(num1))
            {
                try
                {
                    user_input = Console.ReadLine();
                    my_logger.Debug($"num1 input: {user_input}");
                    num1 = Convert.ToDouble(user_input);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad number ...");
                    my_logger.Error($"Error in reading 1st number from user.\nException: {ex}");
                }
            }

            Console.WriteLine("Enter 2nd number:");
            double num2 = double.NaN;
            while (double.IsNaN(num2))
            {
                try
                {
                    user_input = Console.ReadLine();
                    my_logger.Debug($"num2 input: {user_input}");
                    num2 = Convert.ToDouble(user_input);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad number ...");
                    my_logger.Error($"Error in reading 2nd number from user. Exception: {ex}");
                }
            }


            my_logger.Info("******************** System shutdown");
        }
    }
}
