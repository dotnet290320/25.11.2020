﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log4netMssql
{

    class Calculator
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public double Add(double x, double y)
        {
            // info level -- write x and y

            // check if x or y are double.MaxValue
            // if so log error 
            // return double.NaN

            // debug level -- write return value
            return 0;
        }

        public double Div(double x, double y)
        {
            // info level -- write x and y

            // try -catch 
            // error level write exception
            // if divide by zero then return double.NaN

            // observe: logger name should be Calculator!!!!

            // debug level -- write return value
            return 0;

        }
    }
}
