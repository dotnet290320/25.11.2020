﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log4netMssql
{

    class Calculator
    {
        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public double Add(double x, double y)
        {
            // info level -- write x and y
            my_logger.Info($"Add method: {x} / {y}");

            if (x == double.MaxValue)
            {
                my_logger.Error($"Add method: x was max double");
                return double.NaN;
            }

            if (y == double.MaxValue)
            {
                my_logger.Error($"Add method: x was max double");
                return double.NaN;
            }

            // debug level -- write return value
            double result = x + y;
            my_logger.Debug($"Add result : {result}");
            return result;
        }

        public double Div(double x, double y)
        {
            // info level -- write x and y
            my_logger.Info($"Div method: {x} / {y}");

            double result = double.NaN;
            try
            {
                result = x / y;
            }
            catch (Exception  ex)
            {
                my_logger.Error($"Div method: Failed to divide: {x} / {y}.\nException: {ex}");
                Console.WriteLine("Div method: cannot divide by zero");
                return double.NaN;
            }

            my_logger.Debug($"Div method returned {result} ");

            return result;

        }
    }
}
